# Projet Big Data
_Février 2021 - FISE 3 - Info 2_

Par **Corentin Ferlay, Julien Giovinazzo et Kenza Yahiaoui**

# Présention du projet
Voir document PDF "Présentation du projet"

# Documents techniques
Pour plus d'informations sur le modèle de prédiction, voir le document PDF "Doc technique : Machine Learning".  

Pour voir nos résultats de performance en détail, se référer au document PDF "Annexe 1"
